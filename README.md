# Roomle Rubens webhooks

The Roomle Rubens platform gives you the possibility to react to webhooks. To execute a webhook the Roomle Rubens API does a HTTP POST request to a specific URL. This POST request is done with `Content-Type: application/json; charset=utf-8`. Each payload is structured the following way:

```
{
  "event": string,
  "data": JSON Object
}
```

The key "event" is a string which identifies the event. It is basically an event name. The data is dependent on the event. So it can contain different data for different events. When you process a POST request coming from Roomle Rubens API in your webhook there could be two possibilities, either everything went well and your webhook did what you expected or some exception occurred. In the first case just respond with HTTP status code 200 so that the Roomle Rubens API knows that everything went well. In the second case respond with a meaningful HTTP error status code. Then the Roomle Rubens API will fallback to the standard behavior and execute the default action.

In this repository we will demonstrate how to react to the event name `email.savedConfiguration`. Reacting to this event can be helpful to implement a custom logic when the user wants to share a configuration out of Roomle Rubens configurator. In fact you can implement whatever you want but we show how to send a customized e-mail.

To get an idea about the flow, you can have a look at the following flow-chart:

![Web Hook Flow Chart](./docs/WebHookFlowChart.jpg)

To tell the Roomle Rubens API to call your webhook it is important to enable it in Rubens Admin. Therefore just enter your webhook URL in your tenant settings as described [here](https://docs.roomle.com/web/datamanagement/administration.html#webhook). Since this webhook is triggered by the Roomle Rubens configurator it is also important that you use the correct configurator ID. The configurator ID obviously needs to belong to your tenant.

The payload which is posted to our end point in this case looks something like:

```json
{
  "event": "email.savedConfiguration",
  "data": {
    "email": "showcase.roomle@gmail.com",
    "configurationId": "usm:frame:C6A1C58D311C7660962FA07610B1FD076A26F4C860CFF1A84D39626A40CCA8C4",
    "url": "https://www.roomle.com/t/cp/?id=usm:frame:C6A1C58D311C7660962FA07610B1FD076A26F4C860CFF1A84D39626A40CCA8C4&configuratorId=demoConfigurator",
    "configuratorId": "demoConfigurator"
  }
}
```

Of course `email`, `configurationId`, `url`, `configuratorId` are different if the user does something different. Based on `configurationId` you can fetch all the data you need from the Roomle Rubens API. In this example we show how to retrieve the perspective image of a certain configuration. For more details about the Roomle Rubens API we recommend reading the [REST API documentation](https://docs.roomle.com/rapi/RAPIDocumentation.html).
Detailed information about the webhooks can also be found [here](https://docs.roomle.com/rapi/RAPIDocumentation.html#webhooks).

## About this repository

This repository is not intended to be actively developed by externals or to be used as a blueprint for your own webhooks. Nevertheless we think it is helpful to have a working example with real code. Because in this specific example, a code implementation describes better what is needed than written words.

To keep maintenance costs low and enable fast development we decided to create a cloud function within Google Cloud Platform. It is not necessary to understand cloud functions or the Google Cloud Platform to read the code but some conventions are based on the requirements of Google Cloud Platform. The entry point to our cloud function is in `index.js`. This file exports one function which is called `mail`. When a POST to the specified URL happens this function is executed with a `request` and `response` object. From there we collect all the needed data and send the e-mail. It is pretty standard `Node.js` code so it should be easy to read. We did not implement this function to be some production ready code but we applied some best practices like: "never trust user input", "do not add keys and passwords to git" etc. therefore it is not runnable on your machine.

A working curl command can be found in the `package.json`. This demonstrates also the needed headers and the payload.

If there are any questions or if you encounter problems, feel free to open an issue in our [ServiceDesk](http://servicedesk.roomle.com/).
