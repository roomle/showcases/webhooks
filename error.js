// Create a separate Error class so that
// we can output meaningful errors to the
// client
class SanitizedError extends Error {
  constructor(httpCode, message, e = new Error()) {
    super(message);
    // Log errors so we can analyze them better
    console.error(e);
    this.message = message;
    this.httpCode = httpCode;
  }
}

module.exports = {
  SanitizedError,
};
