// Try to read secrets from .env file
// on other platforms we use different
// secret managers
require('dotenv').config();
// Use a library to sanitize input
const validator = require('validator');
// use some lib to parse contentType
// since it has 16mio weekly downloads at time of
// writing we use whatwg-mimetype
const MIMEType = require('whatwg-mimetype');

// Use defined error class which does not expose
// sensitive information if we send out the error message
const {SanitizedError} = require('./error');

const isUtf8 = (string) => {
  if (!string || typeof string !== 'string') {
    throw new SanitizedError(415, 'Can not check charset');
  }
  const charset = string.toLowerCase();
  return charset === 'utf-8' || charset === 'utf8';
};

const getBody = (req) => {
  if (!req || !req.get || typeof req.get !== 'function') {
    throw new SanitizedError(415, 'Malformed request');
  }
  const contentType = req.get('content-type');
  if (!contentType) {
    throw new SanitizedError(415, 'No content type specified');
  }
  let mimeType;
  try {
    mimeType = new MIMEType(contentType);
  } catch (e) {
    throw new SanitizedError(415, 'Can not parse content-type', e);
  }
  if (mimeType.essence === 'application/json') {
    if (mimeType.parameters && typeof mimeType.parameters.get === 'function' && mimeType.parameters.get('charset') && !isUtf8(mimeType.parameters.get('charset'))) {
      throw new SanitizedError(415, 'Currently only charset utf-8 is supported');
    }
    return req.body;
  }
  const contentTypeString = toString(contentType);
  throw new SanitizedError(415, 'Content type: "' + contentTypeString + '" unsupported');
};

const toString = (value) => {
  if (value === undefined) {
    return 'undefined';
  }
  if (value === null) {
    return 'null';
  }
  try {
    return (value.toString) ? value.toString() : value + '';
  } catch (e) {
    throw new SanitizedError(500, 'Can not convert value to string', e);
  }
};

const getEmail = (body) => {
  let email = toString(body.data.email);
  if (!validator.isEmail(email)) {
    throw new SanitizedError(400, 'E-mail is not valid');
  }

  email = validator.normalizeEmail(email);
  return validator.escape(email);
};

const getUrl = (body) => body.data.url ? body.data.url : '';
const getConfigurationId = (body) => body.data.configurationId ? validator.escape(body.data.configurationId) : '';

const isJson = (body) => {
  try {
    JSON.stringify(body);
    return true;
  } catch (e) {
    return false;
  }
};

const isSet = (value) => value !== undefined && value !== null;

const assertValidBody = (req, keys) => {
  if (!req) {
    throw new SanitizedError(500, 'No request received');
  }
  const body = req.body;
  if (!body) {
    throw new SanitizedError(400, 'No body, available, please use HTTP method POST');
  }

  if (!isJson(body)) {
    throw new SanitizedError(400, 'Body is not a valid JSON');
  }

  if (Array.isArray(body)) {
    throw new SanitizedError(400, 'Currently only JSON objects and no JSON arrays are supported');
  }

  for (const key of keys) {
    if (!isSet(body[key])) {
      throw new SanitizedError(400, 'No "' + key + '" provided');
    }
  }
};

const assertValidData = (data, keys) => {
  if (!data) {
    throw new SanitizedError(400, 'No data received');
  }
  for (const key of keys) {
    if (!isSet(data[key])) {
      throw new SanitizedError(400, 'No "' + key + '" provided in data');
    }
  }
};

const assertValidEvent = (event) => {
  if (!event) {
    throw new SanitizedError(400, 'No event defined');
  }
  if (event !== 'email.savedConfiguration') {
    throw new SanitizedError(400, 'Event "' + event + '" is not known');
  }
};

module.exports = {
  assertValidBody,
  assertValidData,
  getBody,
  getEmail,
  toString,
  assertValidEvent,
  getUrl,
  getConfigurationId,
};
